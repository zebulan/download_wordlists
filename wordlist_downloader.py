from bs4 import BeautifulSoup
from os import chdir, getcwd, listdir, makedirs, path
from requests import get
from time import sleep


def packet_storm(url):
    """ Return the correct PacketStorm download URL """
    fd = '/files/download/'
    if fd not in url:
        url = url.replace('/files/', fd)

    filename = url[url.rfind('/')+1:]
    files_in_directory = listdir(getcwd())  # list of files in dir

    if filename not in files_in_directory:  # save only unique files
        if filename.endswith(('.gz', '.zip')):
            r = get(url)
            soup = BeautifulSoup(r.text)
            url = soup.find('a', id='dl-link')['href']  # correct url
        download_file(url, filename)


def download_file(url, filename):
    """ Download a file from a URL and save it to current directory """
    r = get(url, stream=True)
    if r.status_code == 200:
        with open(filename, 'wb') as wordlist:
            for block in r.iter_content(128):  # 1024? can adjust block size
                wordlist.write(block)
    print('Downloaded... {}'.format(filename))
    sleep(2)  # just to be nice...  <-------------- take out ??


def get_word_lists(page_num):
    """ Scrape all the wordlist URLs from PacketStorm RSS (XML) """
    url = 'http://rss.packetstormsecurity.com/Crackers/wordlists/page{}'\
        .format(page_num)
    r = get(url)
    soup = BeautifulSoup(r.text, 'xml')
    if not soup.title.text.startswith('No Results Found'):
        [packet_storm(a.link.text) for a in soup.find_all('item')]
        return True
    return None

wordlists_dir = getcwd() + '\\PacketStorm-Wordlists'
if not path.exists(wordlists_dir):  # skip if directory exists
    makedirs(wordlists_dir)
chdir(wordlists_dir)  # change directory

page_cnt = 1
while True:
    print('Page: {}'.format(page_cnt))
    # add error handling for ConnectionError?  <-----------
    if get_word_lists(page_cnt) is None:
        break
    page_cnt += 1
